# ThorFi Sim

Thorfi Simulator for rapid development.

## Overview

This project uses CadCad [or RadCad which is a wrapper around CadCad] to model the dynamics of the Thorfi system over time.

CadCad is a systems modelling tool, you can learn more [here](https://www.cadcad.education/). We recommend you go through the beginner bootcamp if you are not familiar with CadCad.

Here is a [example of CadCad](https://github.com/cadCAD-org/demos/tree/master/demos/Multiscale/uniswap) used to model Uniswap.

To get an overview of Thorfi, watch this video: https://www.youtube.com/watch?v=qb7NxAbP97U


## Roadmap

### Immediate

Given a steady state with the following assumptions:

- price of assets in the LP is constant
- Fraction of Pool as collateral: 20%
- Fraction of Derived assets in Savings Vaults: 80%
- Savings, lending are roughly balanced.
- Savings yield: it is desireable by the market save 6%+ yield
- CR: it is desireable by the market to borrow upto 250% CR

Q: does rune inflate crazily in the initial blocks?

### Future Scenarios to test

- Savers leave vault en mass
- 2x, 3x price changes
- savings yield and CR demand curves

## Architecture

The model is split into multiple variants each of which will run a different model, with different objectives.
The model variants will share logic and data models that define the thorfi system. These will be available in `model/core` folder

As you can see from the [uniswap diagram](https://github.com/cadCAD-org/demos/tree/master/demos/Multiscale/uniswap), in each model, we have to define

- The environment - users, prices etc. The constraints within which the system operates. These are called params. They are declared in the `params` variable. If they have multiple values, cadcad automatically does multiple runs which allows A/B testing / montecaro runs.
- The states - These change over the course of the simulation. Pool depth, yield etc. They are declared in the `initial_state` variable.
- Policy - logic that takes in the params and the states and computes changes. Each Policy function can output any number of policies which are then fed into the state update functions. They are suffixed with `_policy`
- State update - functions that apply the changes to the state. Each state update can only update one state. They are prefixed by `su_`

## Dev Env Setup

- [Setup jupyter notebook](https://docs.jupyter.org/en/latest/install/notebook-classic.html) (use Anaconda to keep things simple unless you know what you are doing)
- Once the Jupyter server is running, you can view and run the ipynb files on a browser or use the jupyter extension on VSCode.

## Tools

- TODO

## Contributing

- Fork the repo on gitlab.
- Make sure the `trunk check` passes.
- Send a Merge Request to the `develop` branch with a summary of the changes.

This is a rapidly evolving project, please message the devs on thorchain dev discord before you start working on this so we can get you up to speed.

### Linting

We use [trunk](https://docs.trunk.io/), which is a meta-linter. It lints all parts of your code, not just the py files. There is a vscode extension to make life easier.

We can decide to include trunk in a tools folder as part of the repo later.

The linters used by trunk are in `.trunk/trunk.yaml`.

#### Install Using CURL

- `curl https://get.trunk.io -fsSL | bash`

#### Install Using NPM

- `npm install -g @trunkio/launcher`

#### Linting

- Run `trunk check` in the repo folder after trunk is installed to lint your code.
