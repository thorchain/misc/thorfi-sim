from copy import copy
from typing import Tuple

from utils.constants import BLOCK_TIME_SECONDS
from utils.math_utils import calc_apy_from_block_yield

from .model_data_classes import Deposit, ThorUsdVault


def su_btc_pool(params, step, sL, s, _input) -> Tuple:

    """
    state update for btc pool
    """

    new_state = s.copy()
    btc_pool = new_state["btc_pool"]
    input_keys = list(_input.keys())

    if "update_increase_pool_rune_collateral" in input_keys:
        btc_pool.pool_rune_collateral += _input["update_increase_pool_rune_collateral"]
        btc_pool.pool_asset_collateral += _input[
            "update_increase_pool_asset_collateral"
        ]

    if "update_decrease_pool_rune_collateral" in input_keys:
        btc_pool.pool_rune_collateral -= _input["update_decrease_pool_rune_collateral"]
        btc_pool.pool_asset_collateral -= _input[
            "update_decrease_pool_asset_collateral"
        ]

    if "update_slip_pct" in input_keys:
        # Track slip pct to trigger defense.

        hist_slip = btc_pool.historic_slip
        hist_slip.append(_input["update_slip_pct"])

        # update
        btc_pool.historic_slip = hist_slip

    return ("btc_pool", btc_pool)


def su_thor_usd_pool(params, step, sL, s, _input) -> Tuple:

    return ("thor_usd_pool", s["thor_usd_pool"])


####
# Lend Module - state update function
####


def su_lend_module(params, step, sL, s, _input) -> Tuple:

    """
    State update for lend module

    Updates lend_module state for BTC lend module
    """

    new_state = s.copy()
    lend_module = new_state["lend_module"]

    input_keys = list(_input.keys())

    # TODO - simplify by multiplying decresing values by -1
    if "update_increase_pool_debt_minted" in input_keys:
        lend_module.debt_minted += _input["update_increase_pool_debt_minted"]
        lend_module.debt_owed += _input["update_increase_pool_debt_owed"]

    if "update_decrease_pool_debt_minted" in input_keys:
        lend_module.debt_minted -= _input["update_decrease_pool_debt_minted"]
        lend_module.debt_owed -= _input["update_decrease_pool_debt_owed"]

    if "update_cr" in input_keys:

        lend_module.latest_CR = _input["update_cr"]

    if "update_loan_entry" in input_keys:

        # Adds the new loan entry to the `loans` dict.
        # key: user-block, value: dict of loan details
        user_id = _input["update_loan_entry"]["user"]
        timestep = s["timestep"]
        lend_module.loans.update({f"{user_id}-{timestep}": _input["update_loan_entry"]})

    return ("lend_module", lend_module)


def su_rune_supply_tracker_thor_usd(params, step, sL, s, _input) -> Tuple:
    """
    state update for rune supply tracker
    TODO How is this different from rune_supply_tracker? it uses different policy fns.
    Both of these ought to be merge-able, because ultimately they are tracking the same thing.

    """
    rune_supply_tracker = copy(s["rune_supply_tracker"])
    rune_supply_tracker.rune_supply += _input["update_rune_supply"]
    return ("rune_supply_tracker", rune_supply_tracker)


def su_rune_supply_tracker(params, step, sL, s, _input) -> Tuple:
    """
    state update for rune supply tracker
    """
    new_state = s.copy()
    rune_supply_tracker = new_state["rune_supply_tracker"]

    input_keys = list(_input.keys())

    if "update_rune_lend_inflation" in input_keys:

        # TODO - make update_rune_lend_inflation consistent with save_deflation
        # DONT update values in policy
        rune_supply_tracker.rune_lend_inflation += _input["update_rune_lend_inflation"]
        rune_supply_tracker.rune_supply = (
            rune_supply_tracker.rune_supply + _input["update_rune_lend_inflation"]
        )

    if "update_rune_save_deflation" in input_keys:
        rune_supply_tracker.rune_save_deflation += _input["update_rune_save_deflation"]
        rune_supply_tracker.rune_supply = (
            rune_supply_tracker.rune_supply - _input["update_rune_save_deflation"]
        )

    return ("rune_supply_tracker", rune_supply_tracker)


def su_thor_usd_vault(params, step, sL, s, _input) -> Tuple:

    """
    state update for usd_vault
    """

    thor_usd_vault: ThorUsdVault = copy(s["thor_usd_vault"])
    fee_revenue = _input["update_thor_usd_fee_revenue"]
    vault_balance = (
        thor_usd_vault.deposits
        + thor_usd_vault.fee_revenue
        - thor_usd_vault.withdrawals
    )
    # update vault fee revenue
    block_reward_rate = fee_revenue / vault_balance if vault_balance > 0.0 else 0.0
    thor_usd_vault.APY = calc_apy_from_block_yield(
        block_reward_rate, BLOCK_TIME_SECONDS
    )
    # update vault fee revenue
    withdrawals = _input.get("usd_vault_withdrawals", [])
    thor_usd_vault.fee_revenue += fee_revenue
    thor_usd_vault.deposits += _input.get("update_usd_vault_deposit", 0.0)

    new_deposit = _input.get("update_usd_vault_deposit", 0.0)

    thor_usd_vault.members = (
        [Deposit(new_deposit, s["timestep"])] if new_deposit > 0.0 else []
    ) + [
        Deposit(m.amount - withdrawals.get(m.block, 0), m.block)
        for m in thor_usd_vault.members
    ]
    thor_usd_vault.withdrawals += _input.get("update_usd_vault_withdrawal", 0.0)

    return ("thor_usd_vault", thor_usd_vault)


def su_thor_btc_vault(params, step, sL, s, _input) -> Tuple:

    """
    Update Thor.btc vault
    """

    new_state = s.copy()
    btc_vault = new_state["thor_btc_vault"]

    input_keys = list(_input.keys())

    if "update_savings_deposit_amt" in input_keys:
        btc_vault.deposits += _input["update_savings_deposit_amt"]

    # reduce values if there is a withdrawal
    if "update_savings_withdrawal_amt" in input_keys:
        btc_vault.deposits -= _input["update_savings_withdrawal_amt"]
        btc_vault.withdrawals += _input["update_savings_withdrawal_amt"]

    if "update_savings_member" in input_keys:

        btc_vault.members.update(_input["update_savings_member"])

    # TODO - update the vault APY based on the Collateral...

    return ("thor_btc_vault", btc_vault)
